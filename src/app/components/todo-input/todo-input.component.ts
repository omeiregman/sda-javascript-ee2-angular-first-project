import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss'],
})
export class TodoInputComponent implements OnInit {
  constructor() {}

  buttonText = 'Add Item';
  @Output() addTodoEvent = new EventEmitter<unknown>();

  idCounter = 7;

  addItem(newItem) {
    if (!newItem.trim()) {
      alert('You have to input an item');
      return; //exits function
    }

    const newTodoObject = {
      id: this.idCounter,
      name: newItem,
      completed: false,
      assignee: 'Self',
      location: '',
    };
    this.idCounter++; //same thing as writing -->  this.idCounter = this.idCounter + 1;
    this.addTodoEvent.emit(newTodoObject);
  }

  ngOnInit(): void {}
}
