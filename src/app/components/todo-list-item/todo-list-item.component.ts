import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.scss'],
})
export class TodoListItemComponent implements OnInit {
  constructor() {}

  @Input() todoItem;
  @Output() deleteTodoEvent = new EventEmitter<unknown>();

  ngOnInit(): void {}

  setClass() {
    let whatever = {
      todoItem: true,
      'is-complete': this.todoItem.completed,
    };
    return whatever;
  }

  setAssigneeClass() {
    let assigneeClass = {
      'assignee-self': this.todoItem.assignee.toLowerCase() === 'self',
    };
    return assigneeClass;
  }

  showLocation() {
    //One way to do it
    if (this.todoItem.location && !this.todoItem.completed) {
      return true;
    }

    //Another way
    // return this.todoItem.location && !this.todoItem.completed; //Another way.. simple short,

    //Another one
    // if (this.todoItem.location) {
    //   if (this.todoItem.completed) {
    //     return false;
    //   } else {
    //     return true;
    //   }
    // }
  }

  onDelete(todoItem) {
    console.log('Child component: I clicked delete');
    this.deleteTodoEvent.emit(todoItem);
  }

  onCheckboxToggle(todoItem) {
    //You can simply do this..
    todoItem.completed = !todoItem.completed;

    // Or do this.. this might be easier to understand for now..
    // if (todoItem.completed) {
    //   todoItem.completed = false;
    // } else {
    //   todoItem.completed = true;
    // }
  }
}
