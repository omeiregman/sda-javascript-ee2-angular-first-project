import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {
  constructor() {}
  todoItems = [
    {
      id: 1,
      name: 'Grocery shopping',
      completed: true,
      location: 'Maxima',
      assignee: 'sELf',
    },
    {
      id: 2,
      name: 'Study',
      completed: true,
      location: '',
      assignee: 'the husband',
    },
    {
      id: 3,
      name: 'Work',
      completed: true,
      location: '',
      assignee: 'SELF',
    },
    {
      id: 4,
      name: 'Play',
      completed: false,
      location: 'Park',
      assignee: 'the wife',
    },
    {
      id: 5,
      name: 'Laundry',
      completed: true,
      location: 'Lobby',
      assignee: 'Self',
    },
    {
      id: 6,
      name: 'mf',
      location: '',
      completed: false,
      assignee: 'unassigned',
    },
  ];
  renderComponent = true;

  deleteTodo(todoItem) {
    console.log('parent function got the event', todoItem);
    //Logic logic logic
    //TASK.. Find a way to delete this item from the array...
    this.todoItems = this.todoItems.filter((item) => item.id !== todoItem.id);
  }

  addTodo(todoItem) {
    // check if todo Item exists in any object
    //map(), filter(), foreach()
    let isUnique = true;

    this.todoItems.forEach((anyName) => {
      if (anyName.name.toLowerCase() === todoItem.name.toLowerCase()) {
        isUnique = false;
      }
    });

    //AAdd the item to the array
    if (isUnique) {
      this.todoItems.push(todoItem);
    } else {
      alert(`${todoItem.name} already exists in list`);
    }
  }

  ngOnInit(): void {}
}
