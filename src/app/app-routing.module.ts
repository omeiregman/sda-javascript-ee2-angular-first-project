import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SimpleInterestComponent } from './components/simple-interest/simple-interest.component';
import { TitleComponent } from './components/title/title.component';
import { TodosComponent } from './components/todos/todos.component';

const routes: Routes = [
  {
    path: '',
    component: TodosComponent,
  },
  {
    path: 'simple-interest',
    component: SimpleInterestComponent,
  },
  {
    path: 'title',
    component: TitleComponent,
  },
  {
    path: 'new-component',
    component: TodosComponent,
  },
  {
    path: '**',
    redirectTo: '/',
    //component: RenderNotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
